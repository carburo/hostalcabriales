import gulp from "gulp";
import cp from "child_process";
import postcss from "gulp-postcss";
import cssImport from "postcss-import";
import presetEnv from "postcss-preset-env";
import BrowserSync from "browser-sync";
import svgstore from "gulp-svgstore";
import svgmin from "gulp-svgmin";
import inject from "gulp-inject";
import cssnano from "cssnano";

const browserSync = BrowserSync.create();
const defaultArgs = ["-d", "../dist", "-s", "site"];

if (process.env.DEBUG) {
  defaultArgs.unshift("--debug");
}

gulp.task("css", () =>
  gulp
    .src("./src/css/*.css")
    .pipe(
      postcss([
        cssImport({ from: "./src/css/main.css" }),
        presetEnv({ stage: 0 }),
        cssnano(),
      ])
    )
    .pipe(gulp.dest("./dist/css"))
    .pipe(browserSync.stream())
);

gulp.task("hugo", (cb) => buildSite(cb));
gulp.task("hugo-preview", (cb) =>
  buildSite(cb, ["--buildDrafts", "--buildFuture"])
);
gulp.task("build", gulp.series("css", "hugo"));
gulp.task("build-preview", gulp.series("css", "hugo-preview"));

gulp.task("svg", () => {
  const svgs = gulp
    .src("site/static/img/icons-*.svg")
    // .pipe(
    //   svgmin({
    //     plugins: [{ removeViewBox: { active: false } }],
    //   })
    // )
    .pipe(svgmin({
            plugins: [{
                removeDoctype: true
            }, {
                removeXMLNS: true
            }, {
                removeXMLProcInst: true
            }, {
                removeComments: true
            }, {
                removeMetadata: true
            }, {
                removeEditorNSData: true
            }, {
                removeViewBox: false
            }]
        }))
    .pipe(svgstore({ inlineSvg: true }));

  function fileContents(filePath, file) {
    return file.contents.toString();
  }

  return gulp
    .src("site/layouts/partials/svg.html")
    .pipe(inject(svgs, { transform: fileContents }))
    .pipe(gulp.dest("site/layouts/partials/"));
});

gulp.task(
  "server",
  gulp.series("hugo", "css", "svg", () => {
    browserSync.init({
      server: {
        baseDir: "./dist",
      },
    });
    gulp.watch("./src/css/**/*.css", gulp.parallel("css"));
    gulp.watch("./site/static/img/icons-*.svg", gulp.parallel("svg"));
    gulp.watch("./site/**/*", gulp.parallel("hugo"));
  })
);

function buildSite(cb, options) {
  const args = options ? defaultArgs.concat(options) : defaultArgs;

  return cp.spawn("hugo", args, { stdio: "inherit" }).on("close", (code) => {
    if (code === 0) {
      browserSync.reload("notify:false");
      cb();
    } else {
      browserSync.notify("Hugo build failed :(");
      cb("Hugo build failed");
    }
  });
}

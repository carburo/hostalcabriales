---
title: Hostal Cabriales
subtitle: Cozy guest house in Trinidad
image: /img/home-jumbotron
blurb:
  heading: About the house
  text: >-
    Hostal Cabriales is two blocks from central park of Trinidad. The house has
    a fancy and original design. The 4 guest rooms have air conditioned, private
    bathrooms, cold and hot water. Each room has access to a spot like a balcony or a garden,
    where you can relax and enjoy the views. There is also a shared wide terrace, 
    perfect to enjoy with friends and family…
intro:
  heading: What we offer
  text: >-
    We love to make our guests enjoy their stay to the full and for that we offer high quality service.
services:
  - image: /img/illustrations-breakfast.svg
    text: >-
      Breakfasts, lunch and dinner at reasonable prices. Whenever you wish, we will be happy to serve you.
  - image: /img/illustrations-hiking.svg
    text: >-
      City and mountain tours with specialized guides will show the uniqueness
        of Trinidad.
values:
  heading: Our values
  text: >-
    We are a very close family. In our home you will find harmony. We offer calmness, pleasure and good service.
---

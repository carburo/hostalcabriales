convert -resize 350 $1.jpg $1-350w.jpg
convert -resize 480 $1.jpg $1-480w.jpg
convert -resize 600 $1.jpg $1-600w.jpg
convert -resize 800 $1.jpg $1-800w.jpg

squoosh-cli --webp '{}' $1-480w.jpg
squoosh-cli --webp '{}' $1-600w.jpg
squoosh-cli --webp '{}' $1-800w.jpg
squoosh-cli --webp '{}' $1-350w.jpg